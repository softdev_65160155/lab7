/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.usermanagementproject;

import java.util.ArrayList;

/**
 *
 * @author Lenovo
 */
public class UserManagementProject {

    public static void main(String[] args) {
        User admin = new User("admin","Adminstrator","password@123",'M','A');
        User user1 = new User("user1","User1","password@123",'M','U');
        User user2 = new User("user2","User2","password@123",'F','U');
        User user3 = new User("user3","User3","password@123",'F','U');
        System.out.println(admin);
        System.out.println(user1);
        System.out.println(user2);
        User[] userArr = new User[3];
        userArr[0] = admin;
        userArr[1] = user1;
        userArr[2] = user2;
        System.out.println("Print userArr");
        for(int i =0;i<userArr.length;i++){
            System.out.println(userArr[i]);
        }
        System.out.println("ArrayList");
        ArrayList<User> userlist = new ArrayList<User>();
        userlist.add(admin);
        System.out.println(userlist.get(userlist.size()-1)+"List size ="+userlist.size());
        userlist.add(user1);
       System.out.println(userlist.get(userlist.size()-1)+"List size ="+userlist.size());
        userlist.add(user2);
        System.out.println(userlist.get(userlist.size()-1)+"List size ="+userlist.size());
        userlist.add(user3);
        System.out.println(userlist.get(userlist.size()-1)+"List size ="+userlist.size());
        for(int i =0;i<userlist.size();i++){
            System.out.println(userlist.get(i));
        }
        User user4 = new User("user4","User4","password@123",'F','U');
        userlist.set(0,user4);
        userlist.remove(userlist.size()-1);
        System.out.println(userlist.get(0));
        for(User u: userlist){
            System.out.println(u);
        }
    }
    
}
